<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/7/19
 * Time: 4:05 PM
 */

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Response;

class AuthenticateAccess
{
    public function handle($request, Closure $next)
    {
        $validSecrets = explode(',', env('ACCEPTED_SECRETS'));
        if (in_array($request->header('Authorization'), $validSecrets)) {
            return $next($request);
        }
        abort(Response::HTTP_UNAUTHORIZED);
    }
}
