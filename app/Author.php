<?php
/**
 * Created by PhpStorm.
 * User: Yakov
 * Date: 3/7/19
 * Time: 3:58 PM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'name',
        'gender',
        'country',
    ];
}
